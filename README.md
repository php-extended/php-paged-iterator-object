# php-extended/php-paged-iterator-object

A library that implements the php-extended/php-paged-iterator-interface library.


![coverage](https://gitlab.com/php-extended/php-paged-iterator-object/badges/master/pipeline.svg?style=flat-square)
![build status](https://gitlab.com/php-extended/php-paged-iterator-object/badges/master/coverage.svg?style=flat-square)


## Installation

The installation of this library is made via composer and the autoloading of
all classes of this library is made through their autoloader.

- Download `composer.phar` from [their website](https://getcomposer.org/download/).
- Then run the following command to install this library as dependency :
- `php composer.phar php-extended/php-paged-iterator-object ^8`


## Basic Usage

This library provides two classes.

The first one is the `PagedIterator` that is an iterator that also conveys
data about the pagination.

The second one is the `PaginationIterator`, that uses an `PagedDataProviderInterface`
to get back the data under the form of `PagedIterator`s and takes care of
the loops to transform an iterator of PagedIterators into an iterator of the
inner objects itself.


## License

MIT (See [license file](LICENSE)).
