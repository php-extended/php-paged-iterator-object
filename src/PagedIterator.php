<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-paged-iterator-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Iterator;

use IteratorIterator;
use Traversable;

/**
 * PagedIterator class file.
 * 
 * This class is a simple implementation of the PagedIteratorInterface.
 * 
 * @author Anastaszor
 * @template T of object
 * @extends \IteratorIterator<integer|string, T, \Traversable<integer|string, T>>
 * @implements PagedIteratorInterface<T>
 */
class PagedIterator extends IteratorIterator implements PagedIteratorInterface
{
	
	/**
	 * The id of the first page.
	 * 
	 * @var integer
	 */
	protected int $_firstPage;
	
	/**
	 * The id of the current page.
	 * 
	 * @var integer
	 */
	protected int $_currentPage;
	
	/**
	 * The id of the last page.
	 * 
	 * @var integer
	 */
	protected int $_lastPage;
	
	/**
	 * The id of the page step.
	 * 
	 * @var integer
	 */
	protected int $_step;
	
	/**
	 * Builds a new PagedIterator with the given inner traversable and the
	 * pagination data.
	 * 
	 * @param Traversable<integer, T> $iterator
	 * @param integer $currentPage
	 * @param integer $lastPage
	 * @param integer $firstPage
	 * @param integer $step
	 */
	public function __construct(
		Traversable $iterator,
		int $currentPage,
		int $lastPage,
		int $firstPage = 1,
		int $step = 1
	) {
		parent::__construct($iterator);
		$this->_firstPage = $firstPage;
		$this->_currentPage = $currentPage;
		$this->_lastPage = $lastPage;
		$this->_step = $step;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.\spl_object_hash($this);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Iterator\PagedIteratorInterface::getFirstPage()
	 */
	public function getFirstPage() : int
	{
		return $this->_firstPage;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Iterator\PagedIteratorInterface::getPrevPage()
	 */
	public function getPrevPage() : ?int
	{
		$prev = $this->_currentPage - $this->_step;
		if($prev >= $this->_firstPage)
		{
			return $prev;
		}
		
		return null;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Iterator\PagedIteratorInterface::getCurrentPage()
	 */
	public function getCurrentPage() : int
	{
		return $this->_currentPage;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Iterator\PagedIteratorInterface::getNextPage()
	 */
	public function getNextPage() : ?int
	{
		$next = $this->_currentPage + $this->_step;
		if($next <= $this->_lastPage)
		{
			return $next;
		}
		
		return null;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Iterator\PagedIteratorInterface::getLastPage()
	 */
	public function getLastPage() : int
	{
		return $this->_lastPage;
	}
	
}
