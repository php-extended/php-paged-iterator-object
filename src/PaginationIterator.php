<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-paged-iterator-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Iterator;

use Iterator;
use Stringable;

/**
 * PaginationIterator class file.
 * 
 * This iterator represents an iterator that iterates over multiple pages,
 * meaning it retrieves paged iterators through a data provider that is able
 * to do so.
 * 
 * @author Anastaszor
 * @template T of object
 * @implements \Iterator<integer, T>
 */
class PaginationIterator implements Iterator, Stringable
{
	
	/**
	 * The data provider.
	 * 
	 * @var PagedDataProviderInterface<T>
	 */
	protected PagedDataProviderInterface $_dataProvider;
	
	/**
	 * The current iterator.
	 * 
	 * @var ?PagedIteratorInterface<T>
	 */
	protected ?PagedIteratorInterface $_currentIterator;
	
	/**
	 * The current key.
	 * 
	 * @var integer
	 */
	protected int $_currentKey;
	
	/**
	 * The current page number.
	 * 
	 * @var integer
	 */
	protected int $_currentPage;
	
	/**
	 * Builds a new PaginationIterator with the given data provider.
	 * 
	 * @param PagedDataProviderInterface<T> $dataProvider
	 */
	public function __construct(PagedDataProviderInterface $dataProvider)
	{
		$this->_dataProvider = $dataProvider;
		$this->_currentKey = 0;
		$this->_currentPage = 1;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.\spl_object_hash($this);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Iterator::current()
	 * @psalm-suppress InvalidNullableReturnType
	 */
	public function current() : object
	{
		/** @phpstan-ignore-next-line */ /** @psalm-suppress NullableReturnStatement,PossiblyNullReference */
		return $this->_currentIterator->current();
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Iterator::next()
	 */
	public function next() : void
	{
		$this->_currentKey++;
		if(null === $this->_currentIterator)
		{
			// not supposed to be used outside of foreach loops
			// @codeCoverageIgnoreStart
			return;
			// @codeCoverageIgnoreEnd
		}
		
		$this->_currentIterator->next();
		if($this->_currentIterator->valid())
		{
			return;
		}
		
		do
		{
			$this->_currentPage = $this->_currentIterator->getNextPage() ?? $this->_currentPage + 1;
			if($this->_currentIterator->getLastPage() < $this->_currentPage)
			{
				$this->_currentIterator = null;
				
				return;
			}
			
			$this->_currentIterator = $this->_dataProvider->provideData($this->_currentPage);
			$this->_currentIterator->rewind();
		}
		while(!$this->_currentIterator->valid());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Iterator::key()
	 */
	public function key() : int
	{
		return $this->_currentKey;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Iterator::valid()
	 */
	public function valid() : bool
	{
		if(null === $this->_currentIterator)
		{
			return false;
		}
		
		if($this->_currentIterator->getLastPage() > $this->_currentPage)
		{
			return true;
		}
		
		return $this->_currentIterator->valid();
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Iterator::rewind()
	 */
	public function rewind() : void
	{
		$this->_currentKey = 0;
		$this->_currentPage = 1;
		$this->_currentIterator = $this->_dataProvider->provideData($this->_currentPage);
		$this->_currentIterator->rewind();
	}
	
}
