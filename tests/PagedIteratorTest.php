<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-paged-iterator-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\Iterator\PagedIterator;
use PHPUnit\Framework\TestCase;

/**
 * PagedIteratorTest test file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\Iterator\PagedIterator
 *
 * @internal
 *
 * @small
 */
class PagedIteratorTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var PagedIterator
	 */
	protected PagedIterator $_object;
	
	public function testToString() : void
	{
		$this->assertEquals(\get_class($this->_object).'@'.\spl_object_hash($this->_object), $this->_object->__toString());
	}
	
	public function testGetFirstPage() : void
	{
		$this->assertEquals(1, $this->_object->getFirstPage());
	}
	
	public function testGetPrevPage() : void
	{
		$this->assertEquals(1, $this->_object->getPrevPage());
	}
	
	public function testGetPrevNullable() : void
	{
		$this->assertNull((new PagedIterator(new ArrayIterator(), 1, 1))->getPrevPage());
	}
	
	public function testGetCurrentPage() : void
	{
		$this->assertEquals(3, $this->_object->getCurrentPage());
	}
	
	public function testGetNextPage() : void
	{
		$this->assertEquals(5, $this->_object->getNextPage());
	}
	
	public function testGetNextNullable() : void
	{
		$this->assertNull((new PagedIterator(new ArrayIterator(), 1, 1))->getNextPage());
	}
	
	public function testGetLastPage() : void
	{
		$this->assertEquals(5, $this->_object->getLastPage());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new PagedIterator(new ArrayIterator([
			new stdClass(),
		]), 3, 5, 1, 2);
	}
	
}
