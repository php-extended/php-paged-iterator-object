<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-paged-iterator-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\Iterator\PagedDataProviderInterface;
use PhpExtended\Iterator\PagedIterator;
use PhpExtended\Iterator\PagedIteratorInterface;
use PhpExtended\Iterator\PaginationIterator;
use PHPUnit\Framework\TestCase;

/**
 * PaginationIteratorTest test file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\Iterator\PaginationIterator
 *
 * @internal
 *
 * @small
 */
class PaginationIteratorTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var PaginationIterator
	 */
	protected PaginationIterator $_object;
	
	public function testToString() : void
	{
		$this->assertEquals(\get_class($this->_object).'@'.\spl_object_hash($this->_object), $this->_object->__toString());
	}
	
	public function testItWorks() : void
	{
		$count = 0;
		
		foreach($this->_object as $key => $object)
		{
			$this->assertEquals($key, $count);
			$this->assertInstanceOf(stdClass::class, $object, 'loop: '.$count.' (key: '.$key.')');
			$count++;
		}
		
		$this->assertEquals(10, $count);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new PaginationIterator(new class() implements PagedDataProviderInterface
		{
			
			public function __toString() : string
			{
				return self::class.'@'.\spl_object_hash($this);
			}
			
			public function provideData(int $pagenb = 1) : PagedIteratorInterface
			{
				return new PagedIterator(new ArrayIterator([
					new stdClass(),
					new stdClass(),
				]), $pagenb, 5);
			}
			
		});
	}
	
}
